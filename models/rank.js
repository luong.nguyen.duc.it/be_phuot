'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Rank extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ Account }) {
      // define association here
      this.hasMany(Account, { foreignKey: 'Rank_ID', as: 'rankType' });
    }
  }
  Rank.init({
    PointName: DataTypes.STRING,
    Point: DataTypes.INTEGER,
    Discount: DataTypes.INTEGER,
    isActive: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Rank',
  });
  return Rank;
};