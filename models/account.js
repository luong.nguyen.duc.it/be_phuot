'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Account extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ Checkout, Role, Rank }) {
      // define association here
      this.hasMany(Checkout, { foreignKey: 'Account_ID', as: 'Account' });
      this.belongsTo(Role, { foreignKey: 'Role_ID', as: 'roleType' });
      this.belongsTo(Rank, { foreignKey: 'Rank_ID', as: 'rankType' });
    }
  }
  Account.init({
    Username: DataTypes.STRING,
    PhoneNumber: DataTypes.STRING,
    Email: DataTypes.STRING,
    Password: DataTypes.STRING,
    RoleType: DataTypes.STRING,
    Address: DataTypes.STRING,
    Point: DataTypes.INTEGER,
    IsActive: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Account',
  });
  return Account;
};