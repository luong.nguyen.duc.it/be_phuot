'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Accounts', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      Role_ID: {
        allowNull: false,
        references: {
          model: 'Roles',
          key: 'id'
        },
        type: Sequelize.INTEGER
      },
      Rank_ID: {
        allowNull: false,
        references: {
          model: 'Ranks',
          key: 'id'
        },
        type: Sequelize.INTEGER
      },
      Username: {
        allowNull: false,
        type: Sequelize.STRING
      },
      PhoneNumber: {
        type: Sequelize.STRING
      },
      Email: {
        allowNull: false,
        type: Sequelize.STRING
      },
      Password: {
        allowNull: false,
        type: Sequelize.STRING
      },
      RoleType: {
        allowNull: false,
        type: Sequelize.STRING
      },
      Address: {
        type: Sequelize.STRING
      },
      Point: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      IsActive: {
        defaultValue: true,
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Accounts');
  }
};