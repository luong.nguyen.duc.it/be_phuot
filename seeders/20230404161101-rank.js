'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('Ranks', [
      {
        PointName: 'Không xếp hạng',
        Point: 0,
        Discount: 5,
        isActive: true,
        createdAt: '2023-04-04 07:07:31',
        updatedAt: '2023-04-04 07:07:31'
      },
      {
        PointName: 'Bạc',
        Point: 1500,
        Discount: 10,
        isActive: true,
        createdAt: '2023-04-04 07:07:31',
        updatedAt: '2023-04-04 07:07:31'
      },
      {
        PointName: 'Vàng',
        Point: 3000,
        Discount: 15,
        isActive: true,
        createdAt: '2023-04-04 07:07:31',
        updatedAt: '2023-04-04 07:07:31'
      },
      {
        PointName: 'Kim cương',
        Point: 5000,
        Discount: 20,
        isActive: true,
        createdAt: '2023-04-04 07:07:31',
        updatedAt: '2023-04-04 07:07:31'
      },
    ], {});
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Ranks', null, {});
  }
};
