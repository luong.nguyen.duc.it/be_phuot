'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('Accounts', [{
      Username: 'Super Admin',
      PhoneNumber: '0337074546',
      Email: 'spadmin@gmail.com',
      Password: '$2a$12$Oh0SNqPpfWMacBhpzNPfh.82NK/A1KmxftbNvBxfIDujqAsv5LSSm', //'123!@#'
      RoleType: 3,
      Address: 'Hà Nội',
      Point: 0,
      IsActive: 1,
      Role_ID: 3,
      Rank_ID: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }]);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Accounts', null, {});
  }
};
