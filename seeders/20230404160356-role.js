'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('Roles', [
      {
        RoleType: 'ADMIN',
        RoleName: "QUẢN TRỊ VIÊN",
        isActive: true,
        createdAt: '2023-04-04 07:07:31',
        updatedAt: '2023-04-04 07:07:31'
      },
      {
        RoleType: 'CLIENT',
        RoleName: "KHÁCH HÀNG",
        isActive: true,
        createdAt: '2023-04-04 07:07:31',
        updatedAt: '2023-04-04 07:07:31'
      },
      {
        RoleType: 'SUPER_ADMIN',
        RoleName: "SIÊU QUẢN TRỊ VIÊN",
        isActive: true,
        createdAt: '2023-04-04 07:07:31',
        updatedAt: '2023-04-04 07:07:31'
      }
    ], {});
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Roles', null, {});
  }
};
