const express = require('express');
const { TypeUser } = require('../controllers/Role.controller');

const roleRouter = express.Router();

roleRouter.get('/typeUser', TypeUser)

module.exports = {
    roleRouter
}