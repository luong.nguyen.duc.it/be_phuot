const express = require('express');
const { Func_Checkout, Require_Checkout, AllBill, GetBillByID_User, FindDetailBill, getToTalWithMonth, changeStatusAwait,
    changeStatusDelivery,
    changeStatusDone, } = require('../controllers/Checkout.controller');

const checkoutRouter = express.Router();
checkoutRouter.get("/Total/Month/Chart", getToTalWithMonth);
checkoutRouter.post("/RequireCheckout/paypal", Require_Checkout);

checkoutRouter.post('/', Func_Checkout)
checkoutRouter.get('/ByAccount/:id', GetBillByID_User)
checkoutRouter.get('/', AllBill)

checkoutRouter.get('/Detail/:id', FindDetailBill)
checkoutRouter.put("/ChangeStatusAwait", changeStatusAwait);
checkoutRouter.put("/ChangeStatusDelivery", changeStatusDelivery);
checkoutRouter.put("/ChangeStatusDone", changeStatusDone);

module.exports = {
    checkoutRouter
}