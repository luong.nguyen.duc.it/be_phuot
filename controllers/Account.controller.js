const { Account, Role, Rank } = require('../models')
const Sequelize = require('sequelize');
const bcrypt = require('bcryptjs');
const Op = Sequelize.Op;

const getTypeUser = async (id) => {
    const user_type = await Role.findOne({ where: { id } });
    return user_type;
}

const SignUp = async (req, res) => {
    const { Username, Password, Email, RoleType, PhoneNumber, Point, Address } = req.body;
    try {
        const checkEmail = await Account.findOne({ where: { Email } });
        if (!checkEmail) {
            let type = 2;
            if (RoleType) {
                type = RoleType;
            };
            let point = 0;
            if (Point) {
                point = Point
            }
            const salt = bcrypt.genSaltSync(10);
            const hashPassword = bcrypt.hashSync(Password, salt);
            const newAccount = await Account.create({ Username, Password: hashPassword, Role_ID: type, Rank_ID: 1, Email, RoleType: type, Point: point, PhoneNumber, Address });
            res.status(201).send(newAccount);
        } else {
            res.status(403).send("Email không tồn tại!")
        }
    } catch (error) {
        res.status(500).send(error)
    }
}

const SignIn = async (req, res) => {
    const { Email, Password } = req.body
    try {
        const accountSignIn = await Account.findOne({ where: { Email } });
        if (accountSignIn) {
            if (accountSignIn.IsActive) {
                if (bcrypt.compareSync(Password, accountSignIn.Password)) {
                    const user_type = await getTypeUser(accountSignIn.RoleType);
                    accountSignIn.RoleType = user_type;
                    accountSignIn.Password = undefined;
                    res.status(200).send({
                        message: "Login is success",
                        account: accountSignIn
                    })
                } else { // Password sai
                    res.status(200).send("Password sai")
                }
            }
            else {
                res.status(403).send("Tài khoản của bạn đã bị khóa!")
            }
        } else {
            res.status(403).send("Email không tồn tại")
        }
    } catch (error) {
        res.status(500).send(error)
    }
}

const UpdateAccount = async (req, res) => {
    const { Email, Username, Password, PhoneNumber, Address } = req.body;
    const { id } = req.params;
    try {
        const accountUpdate = await Account.findOne({ where: { id } });
        if (accountUpdate) {
            if (Email && Username && Password && PhoneNumber && Address) {
                const salt = bcrypt.genSaltSync(10);
                const hashPassword = bcrypt.hashSync(Password, salt);
                accountUpdate.Username = Username;
                accountUpdate.Email = Email;
                accountUpdate.Password = hashPassword;
                accountUpdate.PhoneNumber = PhoneNumber;
                accountUpdate.Address = Address;
                await accountUpdate.save();
                res.status(200).send(accountUpdate)
            } else {
                res.status(403).send("Data is not enough")
            }
        } else {
            res.status(403).send("Account is not exists")
        }
    } catch (error) {
        res.status(500).send(error)
    }
}

const UpdateAccountPoint = async (req, res) => {
    const { Point } = req.body;
    const { id } = req.params;
    try {
        const accountUpdate = await Account.findOne({ where: { id } });
        if (accountUpdate) {
            if (Point) {
                accountUpdate.Point += Point;
                if (accountUpdate.Point >= 1500 && accountUpdate.Point < 3000 || accountUpdate.Point >= 3000 && accountUpdate.Point < 5000 || accountUpdate.Point >= 5000) {
                    accountUpdate.Rank_ID += 1;
                }
                await accountUpdate.save();
                res.status(200).send(accountUpdate)
            } else {
                res.status(403).send("Data is not enough")
            }
        } else {
            res.status(403).send("Account is not exists")
        }
    } catch (error) {
        res.status(500).send(error)
    }
}

const AllAccount = async (req, res) => {
    const { Username } = req.query;
    try {
        let listAccount;
        if (Username) {
            listAccount = await Account.findAll({
                where: {
                    IsActive: true,
                    Username: {
                        [Op.like]: `%${Username}%`
                    }
                },
                include: [
                    {
                        model: Role,
                        as: 'roleType'
                    }
                ]
            })
        } else {
            listAccount = await Account.findAll({
                where: {
                    IsActive: true,
                },
                include: [
                    {
                        model: Role,
                        as: 'roleType'
                    },
                    {
                        model: Rank,
                        as: 'rankType'
                    }
                ]
            });
        }
        listAccount.forEach(element => {
            element.Password = undefined;
            element.RoleType = undefined;
        });
        res.status(200).send(listAccount)
    } catch (error) {
        res.status(500).send(error)
    }
}
const DetailAccount = async (req, res) => {
    const { id } = req.params;
    try {
        const detailAccount = await Account.findOne({
            where: { id, IsActive: true },
            include: [
                {
                    model: Role,
                    as: 'roleType'
                },
                {
                    model: Rank,
                    as: 'rankType'
                }
            ]
        })
        detailAccount.Password = undefined;
        detailAccount.RoleType = undefined;
        res.status(200).send(detailAccount)
    } catch (error) {
        res.status(500).send(error)
    }
}
const DeleteAccount = async (req, res) => {
    const { id } = req.params;
    try {
        const detailAccount = await Account.findOne({ where: { id, IsActive: true } })
        if (detailAccount) {
            detailAccount.IsActive = false;
            await detailAccount.save();
            res.status(200).send(detailAccount)
        } else {
            res.status(404).send("Account is not exists")
        }
    } catch (error) {
        res.status(500).send(error)
    }
}
module.exports = {
    SignUp,
    SignIn,
    UpdateAccount,
    UpdateAccountPoint,
    AllAccount,
    DetailAccount,
    DeleteAccount,
}