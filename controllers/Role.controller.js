const { Role } = require('../models')
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

const TypeUser = async (req, res) => {
    const { RoleType } = req.body;
    try {
        const userType = await Role.findOne({ where: { id: RoleType, isActive: true } });
        if (userType) {
            res.status(200).send(userType)
        } else {
            res.status(404).send("Role is not exists")
        }
    } catch (error) {
        res.status(500).send(error)
    }
}

module.exports = {
    TypeUser,
}